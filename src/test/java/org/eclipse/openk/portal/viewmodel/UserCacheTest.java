/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.viewmodel;


import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.eclipse.openk.portal.auth2.model.KeyCloakUser;
import org.eclipse.openk.portal.common.JsonGeneratorBase;
import org.eclipse.openk.portal.common.util.ResourceLoaderBase;
import org.junit.Test;

public class UserCacheTest extends ResourceLoaderBase {
    // IMPORTANT TEST!!!
    // Make sure, our Interface produces a DEFINED Json!
    // Changes in the interface will HOPEFULLY crash here!!!

    @Test
    public void testStructureAgainstJsonAndSettersAndGetters() {
        String json = super.loadStringFromResource("testKeyCloakUsers.json");
        KeyCloakUser[] keyCloakUsers = JsonGeneratorBase.getGson().fromJson(json, KeyCloakUser[].class);
        List<KeyCloakUser> keyCloakUsersList = new ArrayList<>(Arrays.asList(keyCloakUsers));
        UserCache uc = UserCache.getInstance();
        uc.setKeyCloakUsers(keyCloakUsersList);

        assertTrue(keyCloakUsers.length == 3);
        assertTrue(uc.getKeyCloakUsers().get(0).getFirstName().equals("Administrator"));
        assertTrue(uc.getKeyCloakUsers().get(2).getUsername().equals("otto"));

    }
}
