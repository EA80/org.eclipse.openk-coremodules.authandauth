/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.lang.annotation.Annotation;
import java.net.URI;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import org.eclipse.openk.portal.common.Globals;
import org.eclipse.openk.portal.exceptions.PortalBadRequest;
import org.eclipse.openk.portal.exceptions.PortalException;
import org.eclipse.openk.portal.exceptions.PortalUnauthorized;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class BaseWebServiceTest {
	private static final org.apache.log4j.Logger EMPTYLOGGER = org.apache.log4j.Logger
			.getLogger(BaseWebServiceTest.class.getName());

	public static class TestWebService extends BaseWebService {
		public String sessionId;
		public boolean throwUnauthException = false;

		public TestWebService() {
			super(EMPTYLOGGER);
		}

		protected void assertAndRefreshToken(String sessionId, boolean refresh) throws PortalException {
			if (throwUnauthException) {
				throw new PortalUnauthorized();
			} else {
				this.sessionId = sessionId;
			}
		}
	}

	public static class TestInvokable extends BackendInvokable {
		public boolean isInvoked = false;
		public PortalException exceptionToThrow = null;
		public boolean throwRuntime = false;
		public Response response = null;

		@Override
		public Response invoke() throws PortalException {
			if (exceptionToThrow != null) {
				throw exceptionToThrow;
			}
			if (throwRuntime) {
				((String) null).equals("error"); //NOSONAR
			}
			isInvoked = true;
			return response;
		}
	}
	
	@Test
	public void testInvokeException() {
		TestWebService tws = new TestWebService();
		TestInvokable ti = new TestInvokable();

		ti.exceptionToThrow = new PortalBadRequest();
		Response ret = tws.invoke("ssess", false, ti);
		Assert.assertEquals(Globals.HTTPSTATUS_BAD_REQUEST, ret.getStatus());
	}

	@Test
	public void testInvokeRuntimeException() {
		TestWebService tws = new TestWebService();
		TestInvokable ti = new TestInvokable();

		ti.throwRuntime = true;
		Response ret = tws.invoke("ssess",  false, ti);
		assertEquals(Globals.HTTPSTATUS_INTERNAL_SERVER_ERROR, ret.getStatus());
	}

	@Test
	public void testUnauthException() {
		TestWebService tws = new TestWebService();
		TestInvokable ti = new TestInvokable();
		tws.throwUnauthException = true;
		Response ret = tws.invoke("ssess",  false, ti);
		assertEquals(Globals.HTTPSTATUS_UNAUTHORIZED, ret.getStatus());
	}

	@Test
	public void testSecureTypeNone() {
		TestWebService tws = new TestWebService();
		TestInvokable ti = new TestInvokable();
		Response responseForTypeNONE = tws.invoke("ssess", false, ti);
		assertNull(responseForTypeNONE);
	}


	@Test
	public void testInvokation() {
		TestWebService tws = new TestWebService();
		TestInvokable ti = new TestInvokable();
		ti.response = new Response() {
			@Override
			public Object getEntity() {
				return null;
			}

			@Override
			public <T> T readEntity(Class<T> aClass) {
				return null;
			}

			@Override
			public <T> T readEntity(GenericType<T> genericType) {
				return null;
			}

			@Override
			public <T> T readEntity(Class<T> aClass, Annotation[] annotations) {
				return null;
			}

			@Override
			public <T> T readEntity(GenericType<T> genericType, Annotation[] annotations) {
				return null;
			}

			@Override
			public boolean hasEntity() {
				return false;
			}

			@Override
			public boolean bufferEntity() {
				return false;
			}

			@Override
			public void close() {

			}

			@Override
			public MediaType getMediaType() {
				return null;
			}

			@Override
			public Locale getLanguage() {
				return null;
			}

			@Override
			public int getLength() {
				return 0;
			}

			@Override
			public Set<String> getAllowedMethods() {
				return null;
			}

			@Override
			public Map<String, NewCookie> getCookies() {
				return null;
			}

			@Override
			public EntityTag getEntityTag() {
				return null;
			}

			@Override
			public Date getDate() {
				return null;
			}

			@Override
			public Date getLastModified() {
				return null;
			}

			@Override
			public URI getLocation() {
				return null;
			}

			@Override
			public Set<Link> getLinks() {
				return null;
			}

			@Override
			public boolean hasLink(String s) {
				return false;
			}

			@Override
			public Link getLink(String s) {
				return null;
			}

			@Override
			public Link.Builder getLinkBuilder(String s) {
				return null;
			}

			@Override
			public int getStatus() {
				return 666;
			}

			@Override
			public StatusType getStatusInfo() {
				return null;
			}

			@Override
			public MultivaluedMap<String, Object> getMetadata() {
				return null;
			}

			@Override
			public MultivaluedMap<String, String> getStringHeaders() {
				return null;
			}

			@Override
			public String getHeaderString(String s) {
				return null;
			}
		};

		Response ret = tws.invoke("ssess",  false, ti);
		assertTrue(ti.isInvoked);
		assertEquals("ssess", tws.sessionId);
		assertEquals(666, ret.getStatus() );

	}

}
