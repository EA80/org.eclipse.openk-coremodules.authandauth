/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.controller;


import org.eclipse.openk.portal.auth2.model.KeyCloakUser;
import java.util.List;
import javax.ws.rs.core.Response;
import org.eclipse.openk.portal.common.JsonGeneratorBase;
import org.eclipse.openk.portal.exceptions.PortalException;
import org.eclipse.openk.portal.exceptions.PortalExceptionMapper;
import org.eclipse.openk.portal.viewmodel.UserCache;
import org.eclipse.openk.portal.viewmodel.UserModule;
import org.eclipse.openk.portal.viewmodel.VersionInfo;


public class ControllerImplementations
{
    private ControllerImplementations() {}

    public static class GetUsers extends BackendInvokable {

        @Override
        public Response invoke() throws PortalException {
            List<KeyCloakUser> keyCloakUserList = UserCache.getInstance().getKeyCloakUsers();
            return ResponseBuilderWrapper.INSTANCE.buildOKResponse(JsonGeneratorBase.getGson().toJson(keyCloakUserList));
        }
    }

    public static class GetVersionInfo extends BackendInvokable {
        private BackendController backendController;
        
        public GetVersionInfo(BackendController backendController) {
            this.backendController = backendController;
        }
        
        @Override
        public Response invoke() throws PortalException {
            VersionInfo vi = backendController.getVersionInfo();
            return ResponseBuilderWrapper.INSTANCE.buildOKResponse(JsonGeneratorBase.getGson().toJson(vi));
        }
    }

    public static class Logout extends BackendInvokable {
        private String accessToken;

        public Logout(String accessToken) {
            this.accessToken = accessToken;
        }

        @Override
        public Response invoke() throws PortalException {
            TokenManager.getInstance().logout(accessToken);
            return ResponseBuilderWrapper.INSTANCE.buildOKResponse(PortalExceptionMapper.getGeneralOKJson());
        }
    }

    public static class CheckAuth extends BackendInvokable {

        public CheckAuth() {
            /* the implemented invoke method is only called after a successfully
            pass of assertAndRefreshToken method of the BaseWebService class which checks
            if the Token is active and valid */
        }

        @Override
        public Response invoke() throws PortalException {
		        //see constructor above
		        return ResponseBuilderWrapper.INSTANCE.buildOKResponse(PortalExceptionMapper.getGeneralOKJson());
        }
    }

    public static class GetUserModulesForUser extends BackendInvokable {
        private BackendController backendController;

        public GetUserModulesForUser(BackendController backendController) {
            this.backendController = backendController;
        }

        @Override
        public Response invoke() throws PortalException {
            List<UserModule> um = backendController.getUserModuleList();
            return ResponseBuilderWrapper.INSTANCE.buildOKResponse(JsonGeneratorBase.getGson().toJson(um));
        }
    }

    public static class GetUsersForRole extends BackendInvokable {
        private BackendController backendController;
        private String userRole;

        public GetUsersForRole(BackendController backendController, String userRole) {
            this.backendController = backendController;
            this.userRole = userRole;
        }

        @Override
        public Response invoke() throws PortalException {
            List<KeyCloakUser> keyCloakUserList = backendController.getUsersForRole(userRole);
            return ResponseBuilderWrapper.INSTANCE.buildOKResponse(JsonGeneratorBase.getGson().toJson(keyCloakUserList));
        }
    }
}
