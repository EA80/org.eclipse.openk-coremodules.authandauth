/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
package org.eclipse.openk.portal.common.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.ByteOrderMark;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BOMInputStream;
import org.apache.log4j.Logger;

public class ResourceLoaderBase {

    private static final Logger LOGGER = Logger.getLogger(ResourceLoaderBase.class.getName());

    private String stream2String(InputStream is) {
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(is, writer, "UTF-8");
        } catch (Exception e) { // NOSONAR
            return "";
        }
        return writer.toString();
    }

    private String stream2String(InputStream is, String filename) {
        StringWriter writer = new StringWriter();

        try (BOMInputStream bomInputStream = new BOMInputStream(is, false,
                ByteOrderMark.UTF_8, ByteOrderMark.UTF_16BE, ByteOrderMark.UTF_16LE,
                ByteOrderMark.UTF_32BE, ByteOrderMark.UTF_32LE)) {
            IOUtils.copy(bomInputStream, writer, StandardCharsets.UTF_8.name());
        } catch (IOException e) {
            LOGGER.error("Fehler in stream2String()", e);
            return "";
        }

        LOGGER.debug("Datei erfolgreich eingelesen: " + filename);
        return writer.toString();
    }

    public String loadStringFromResource(String filename) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream jsonstream = classLoader.getResourceAsStream(filename);
        if (jsonstream==null){
            LOGGER.error("Datei nicht gefunden: " + filename);
            return null;
        }
        return stream2String(jsonstream);
    }

    public String loadFromPath(String path) {
        try {
            Path paths = Paths.get(path);
            LOGGER.debug("paths: " + path);
            try (InputStream inputStream = Files.newInputStream(Paths.get(path))) {
                return stream2String(inputStream, paths.getFileName().toString());
            }
        } catch (IOException e) {
            LOGGER.error("Fehler in loadFromPath", e);
            return null;
        }
    }
}
