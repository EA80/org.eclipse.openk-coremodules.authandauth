/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.controller;

import org.apache.log4j.Logger;
import org.eclipse.openk.portal.common.Globals;
import org.eclipse.openk.portal.common.JsonGeneratorBase;
import org.eclipse.openk.portal.exceptions.PortalBadRequest;
import org.eclipse.openk.portal.exceptions.PortalUnauthorized;
import org.eclipse.openk.portal.viewmodel.LoginCredentials;

public class InputDataValuator {
    private static final Logger LOGGER = Logger.getLogger(InputDataValuator.class.getName());

    private static final String WHITELIST = "[^a-zA-ZäÄöÖüÜß?0-9().,-:;_+=!%§&/'#<>\" ]";

    private void checkCredentialsRaw(String credentials) throws PortalUnauthorized {
        if (credentials == null || credentials.isEmpty()) {
            throw new PortalUnauthorized("No credentials provided");
        }
        if (credentials.length() > Globals.MAX_CREDENTIALS_LENGTH) {
            LOGGER.warn("MaxLength of credentials exceeded");

            throw new PortalUnauthorized("Invalid credentials");
        }
    }

    public void checkCredentials(String credentials) throws PortalUnauthorized {
        checkCredentialsRaw(credentials);

        LoginCredentials obj;
        try {
            obj = JsonGeneratorBase.getGson().fromJson(credentials, LoginCredentials.class);
        } catch (Exception e) { // NOSONAR
            obj = null;
        }
        if (obj == null || obj.getUserName() == null || obj.getUserName().isEmpty()) {
            LOGGER.warn("Invalid credentials provided. ");
            throw new PortalUnauthorized("Invalid credentials");
        }
    }

    private void checkWhitelistChars(String txt) throws PortalBadRequest { // NOSONAR 24.09.2018: There's a great possibility that we need a whitebox-charecter check for security reasons again. Because of this, we leave this code
        checkWhitelistChars(txt, false);
    }

    private void checkWhitelistChars(String txt, boolean logTextOnError) throws PortalBadRequest {
        // empty String is ok
        if (txt == null || txt.isEmpty()) {
            return;
        }
        String tx2 = txt.replaceAll(WHITELIST, "");
        if (!tx2.equals(txt)) {
            LOGGER.warn("Invalid text not matching whitelist" + (logTextOnError ? ":" + txt : ""));
            throw new PortalBadRequest("Invalid text data");
        }
    }
}
