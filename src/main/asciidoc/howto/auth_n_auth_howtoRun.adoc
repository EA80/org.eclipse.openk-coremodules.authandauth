////
******************************************************************************
* Copyright © 2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
*
*     http://www.eclipse.org/legal/epl-v10.html
*
******************************************************************************
////
= openKonsequenz - How to run the module "portal@openK"
:Date: 2017-11-17
:Revision: 1
:icons:
:source-highlighter: highlightjs
:highlightjs-theme: solarized_dark

<<<

== Requirements
* Browser (Chrome or IE11 suggested)

== Prerequisites

* KeyCloak 3.2.1 Final (look at the *http://87.128.212.254:8880/portalFE_ADoc/auth_n_auth_architectureDocumentation.html[auth_n_auth_architectureDocumentation]* in *Deployment View* Chapter for install instructions)

IMPORTANT: KeyCloak must be installed for a successful login at the application!

== How to run the Backend
To run the backend you need to have installed and configured Apache Tomcat (look at *auth_n_auth_howtoBuild*).

=== Set up and start Apache Tomcat
Tomcat needs the Web Application Archive (war) file, which is produced by building the maven project, to run the application.

* Copy the *portal.war* file from the project file `/target` in the `<tomcat>/webapps` file. If there is a folder named *portal*, delete it before.
* Navigate to `C:\apache-tomcat-8.0.30\bin` and start Tomcat by clicking on *startup.bat*.
* Tomcat's default port is *8080*.

[source,text]
----
If 8080 port is already in use, you have to change tomcat's port as follows:
- Go to <tomcat>/conf folder
- Open server.xml file and search for "Connector port"
- Replace "8080" by your port number (for example 8181)
- Restart tomcat server (in the same folder which startup.bat is located, you can also find a shutdown.bat file).
----
{blank}

IMPORTANT: If you change the port number you have also adapt the port in the
frontend project: File "<PRJ_portalFE>/proxy.conf.json". There might be other config files
that need to be adepted as well!

TIP: Look at the *http://87.128.212.254:8880/portalFE_ADoc/auth_n_auth_interfaceDocumentation.html[auth_n_auth_interfaceDocumentation]* for informations about the services.

== How to run the Frontend
To run the frontend project you need to have installed and updated Node.js and npm Angular-CLI.

=== Compile the Frontend

To compile say Angular-CLI to start.

* Open a command line and navigate to the root folder of the frontend project
* Run the command

[source,command]
----
   $  npm start
----
{blank}

* Open a browser and type:

[source,http]
----
    http://localhost:4201
----
{blank}

IMPORTANT: To open the *Betriebstagebuch* module you have to run the *elogbook* application too.

